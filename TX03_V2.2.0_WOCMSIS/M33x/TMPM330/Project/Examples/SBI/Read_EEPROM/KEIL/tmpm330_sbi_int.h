/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_sbi_int.h
* @brief   all interrupt request functions prototypes of Read EEPROM demo
*          for the TOSHIBA 'TMPM330' Device Series 
* @version V1.200
* @date    2010/6/23
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TMPM330_SBI_INT_H
#define __TMPM330_SBI_INT_H
/* Includes ------------------------------------------------------------------*/
#include "TMPM330.h"
#include "tmpm330_sbi.h"

extern uint32_t gI2C_WCnt;
extern uint32_t gI2C_RCnt;
extern uint32_t gI2C_TxDataLen;
extern uint32_t gI2C_TxData[16];
extern uint32_t gI2C_RxDataLen;
extern uint32_t gI2C_RxData[16];

extern SBI_InitI2CTypeDef myI2C;

void INTSBI2_IRQHandler(void);

#endif                          /* __TMPM330_SBI_INT_H */
