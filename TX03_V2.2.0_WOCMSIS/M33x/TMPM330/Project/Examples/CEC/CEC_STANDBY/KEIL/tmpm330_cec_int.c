/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_cec_int.c
* @brief   all interrupt request functions definition of CEC standby demo
*          for the TOSHIBA 'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/24
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_cec_int.h"

/**
  * @brief  The CEC Receive interrupt  function.
  * @param  None
  * @retval None
  */

typedef enum {
    DATA_INIT = 0,
    DATA_IN_PROGRESS = 1,
    DATA_END = 2
} CEC_DataState;

typedef struct {
    CEC_LogicalAddr Initiator;
    CEC_LogicalAddr Destination;
    uint8_t Opcode;
    uint8_t CEC_Data[17];
    uint8_t current_num;
    uint8_t Max_num;
    CEC_DataState current_state;
} CEC_FrameTypeDef;

extern CEC_FrameTypeDef CEC_RxFrameTmp;
extern CEC_FrameTypeDef CEC_TxFrame;
/**
  * @brief  The CEC reception interrupt  function.
  * @param  None
  * @retval None
  */
void INTCECRX_IRQHandler(void)
{
    CEC_DataTypeDef data;
    CEC_RxINTState stateRx;
    TSB_CG->ICRCG = 0x06U;      /* CEC clear interrupt */
    data = CEC_GetRxData();
    stateRx = CEC_GetRxINTState();
    if (stateRx.Bit.RxEnd == 1) {
        if (CEC_RxFrameTmp.current_num < 17U) {
            CEC_RxFrameTmp.CEC_Data[CEC_RxFrameTmp.current_num] = data.Data;
            CEC_RxFrameTmp.current_num++;
            CEC_RxFrameTmp.current_state = DATA_IN_PROGRESS;
        }
        if (data.EOMBit == CEC_EOM) {
            CEC_RxFrameTmp.current_state = DATA_END;
        }
    } else if (stateRx.Bit.RxStartBit == 1) {
        /*Do nothing */
    } else {
        CEC_RxFrameTmp.current_state = DATA_END;
    }

}

/**
  * @brief  The CEC transmission interrupt  function.
  * @param  None
  * @retval None
  */
void INTCECTX_IRQHandler(void)
{
    CEC_TxINTState stateTx; 
    TSB_CG->ICRCG = 0x0cU;      /* CEC clear interrupt */
    stateTx = CEC_GetTxINTState();
    if (stateTx.Bit.TxStart == 1) {
        CEC_TxFrame.current_num++;
        if ((CEC_TxFrame.current_num + 1U) >= CEC_TxFrame.Max_num) {
            /* send the next byte of frame */
            CEC_SetTxData(CEC_TxFrame.CEC_Data[CEC_TxFrame.current_num], CEC_EOM);
        } else {
            /* send the last byte of frame */
            CEC_SetTxData(CEC_TxFrame.CEC_Data[CEC_TxFrame.current_num], CEC_NO_EOM);
        }
    } else {
        CEC_TxFrame.current_state = DATA_END;
    }
}

