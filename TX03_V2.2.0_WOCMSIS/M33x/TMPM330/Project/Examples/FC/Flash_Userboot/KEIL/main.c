/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   main function of Flash demo(user boot mode) for TMPM330FDFG
* @version V0.100
* @date    2010/12/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#include "main.h"
#include "tmpm330_uart.h"

void (*startup) (void);

#if defined ( __CC_ARM   )      /* RealView Compiler */
/**
  * @brief  Get store base of "FLASHSWAP" section
  * @param  None.
  * @retval The base.
  */
__ASM unsigned int __Get_FLASHSWAP_Store_Base(void)
{ 
    IMPORT ||Load$$FLASHSWAP_RAM$$Base|| 
    LDR R0, =||Load$$FLASHSWAP_RAM$$Base|| 
    BX LR
} 

/**
  * @brief  Get run base of "FLASHSWAP" section
  * @param  None.
  * @retval The base.
  */
__ASM unsigned int __Get_FLASHSWAP_Run_Base(void)
{
    IMPORT ||Image$$FLASHSWAP_RAM$$RO$$Base|| 
    LDR R0, =||Image$$FLASHSWAP_RAM$$RO$$Base|| 
    BX LR
} 		 

/**
  * @brief  Get run limit of "FLASHSWAP" section
  * @param  None.
  * @retval The limit.
  */
__ASM unsigned int __Get_FLASHSWAP_Run_Limit(void)
{
    IMPORT ||Image$$FLASHSWAP_RAM$$RO$$Limit||
    LDR R0, =||Image$$FLASHSWAP_RAM$$RO$$Limit|| 
    BX LR
} 

#endif

extern uint8_t uart_copy(void);
extern uint32_t rec_data[512];

uint8_t TxBuffer[] = "TMPM330";

uint8_t TxCounter = 0U;
uint8_t RxCounter = 0U;

/**
  * @brief  main function to run flash userboot mode demo
  * @param  None.
  * @retval result to retun to system.
  */
int main(void)
{
	uint32_t reg_value = 0U;
    uint32_t  FLASHSWAP_Store_Base;
    uint32_t  FLASHSWAP_Run_Base;
    uint32_t  FLASHSWAP_Run_Limit;

	CG_SetPLL(DISABLE);         /* Disable PLL */
    CG_SetFcSrc(CG_FC_SRC_FOSC);        /* Select fosc */
    GPIO_SetInput(GPIO_PB, GPIO_BIT_4); /* set port B to input */

    if (Mode_Judgement() == USER_BOOT_MODE) {   /* if switch is low, enter user boot mode */

        FLASHSWAP_Store_Base = __Get_FLASHSWAP_Store_Base();
        FLASHSWAP_Run_Base = __Get_FLASHSWAP_Run_Base();
        FLASHSWAP_Run_Limit = __Get_FLASHSWAP_Run_Limit();

		/* copy programing routine from Flash to RAM for execution. */
		Copy_Routine((uint32_t *)FLASHSWAP_Run_Base, (uint32_t *)FLASHSWAP_Store_Base, (FLASHSWAP_Run_Limit - FLASHSWAP_Run_Base));

		uart_copy();

		/* Vector reset */
	    reg_value = SCB->AIRCR;
	    reg_value = (uint32_t) 0x05FA0001;
	    SCB->AIRCR = reg_value;
    } else {                    /* if switch is high, enter normal mode */

		/* Change Vector Table offset */
	    SCB->VTOR = DEMO_A_FLASH;

        startup = (void *)(*(unsigned int*)(DEMO_A_FLASH + 4));
        startup();              /* jump to code start address to run */
    }
    return 0;

}



/**
  * @brief  judge current mode is USER_BOOT_MODE or NORMAL_MODE
  * @param  None.
  * @retval None.
  */
uint8_t Mode_Judgement(void)
{
    return (GPIO_ReadDataBit(GPIO_PB, GPIO_BIT_4) ==
            GPIO_BIT_VALUE_0) ? USER_BOOT_MODE : NORMAL_MODE;
}

/**
  * @brief  copy 32-bit data from source to dest
  * @param  the address of source and dast, the data size
  * @retval None.
  */
void Copy_Routine(uint32_t * dest, uint32_t * source, uint32_t size)
{
    uint32_t *dest_addr, *source_addr, tmpsize;
    uint32_t i, tmps, tmpd, mask;

    dest_addr = dest;
    source_addr = source;

    tmpsize = size >> 2U;

    for (i = 0U; i < tmpsize; i++) {    /* 32bits copy */
        *dest_addr = *source_addr;
        dest_addr++;
        source_addr++;
    }
    if (size & 0x00000003U) {   /* if the last data size is not 0(maybe 1,2 or 3), copy the last data */
        mask = 0xFFFFFF00U;
        i = size & 0x00000003U;
        tmps = *source_addr;
        tmpd = *dest_addr;
        while (i - 1U) {
            mask = mask << 8U;
            i--;
        }
        tmps = tmps & (~mask);
        tmpd = tmpd & (mask);
        *dest_addr = tmps + tmpd;       /* 32bits copy, but only change the bytes need to be changed */
    } else {
        /* Do nothing */
    }
}


/**
  * @brief  write flash
  * @param  destination address, source address, lenth of data.
  * @retval ERROR or SUCCESS.
  */
#if defined ( __CC_ARM   )      /* RealView Compiler */
#pragma arm section code="FLASH_ROM"
#endif
uint8_t Write_Flash(uint32_t * addr_dest, uint32_t * addr_source, uint32_t len)
{
    uint32_t size;
    uint32_t *source;
    uint32_t *dest;

    dest = addr_dest;
    source = addr_source;
    size = len;
    while (size > PAGE_SIZE) {
        if (FC_SUCCESS == FC_WritePage((uint32_t) dest, source)) {      /* write one page every time */
            /* Do nothing */
        } else {
            return ERROR;
        }
        size = size - PAGE_SIZE;
        dest = dest + PAGE_SIZE / 4U;
        source = source + PAGE_SIZE / 4U;
    }
    if (FC_SUCCESS == FC_WritePage((uint32_t) dest, source)) {  /* write the last data, no more than one page */
        /* Do nothing */
    } else {
        return ERROR;
    }
    return SUCCESS;
}	   

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif

/*************************** END OF FILE **************************************/
