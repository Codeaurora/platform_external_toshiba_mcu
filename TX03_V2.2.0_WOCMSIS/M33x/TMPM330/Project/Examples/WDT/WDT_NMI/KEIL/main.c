/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   The application functions of WDT demo for the
*          TOSHIBA 'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/18
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#include "tmpm330_wdt.h"
#include "led.h"
/*#define DEMO2*/
void Delay(int time);
void Delay(int time)
{
    volatile int i = 0;
    for ( i = 0; i < (time * 800); ++i);
}

int main(void)
{
    WDT_InitTypeDef WDT_InitStruct;
    WDT_InitStruct.DetectTime = WDT_DETECT_TIME_EXP_25;
    WDT_InitStruct.OverflowOutput = WDT_NMIINT;

    SystemInit();

    LED_Configuration();
    WDT_Init(&WDT_InitStruct);
    WDT_Enable();

    while (1) {
#ifdef DEMO2
        Delay(500);
        WDT_WriteClearCode();
        LED_Toggle(LED3);
#endif
    }
}

#ifdef DEBUG
void assert_failed(char* file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
