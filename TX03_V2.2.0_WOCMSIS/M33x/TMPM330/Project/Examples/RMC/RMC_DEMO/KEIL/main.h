/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.h
* @brief   the application functions of RMC (Remote Controller) demo for the
*          TOSHIBA 'TMPM330' Device Series
* @version V2.000
* @date    2010/06/17
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_rmc.h"
#include "tmpm330_rmc_int.h"

#define TOSHIBA_FORMAT_RMC
//#define RC5_FORMAT_RMC

#ifdef TOSHIBA_FORMAT_RMC
#define RMC_MAX_CYCLE                   ((uint8_t)0x71)
#define RMC_MIN_CYCLE                   ((uint8_t)0x5A)
#define RMC_MAX_LOW_WIDTH               ((uint8_t)0x4C)
#define RMC_MIN_LOW_WIDTH               ((uint8_t)0x49)
#define RMC_TRG_LOW_WIDTH               ((uint8_t)0xFF)
#define RMC_TRG_MAX_DATA_BIT_CYCLE      ((uint8_t)0xFE)
#define RMC_LARGER_THRESHOLD            ((uint8_t)0x00)
#define RMC_SMALLER_THRESHOLD           ((uint8_t)0x2E)
#define RMC_NOISE_CANCELLATION_TIME     ((uint8_t)0x0D)
#endif

#ifdef RC5_FORMAT_RMC
#define RMC_MAX_CYCLE                   ((uint8_t)0x00)
#define RMC_MIN_CYCLE                   ((uint8_t)0x00)
#define RMC_MAX_LOW_WIDTH               ((uint8_t)0x00)
#define RMC_MIN_LOW_WIDTH               ((uint8_t)0x00)
#define RMC_TRG_LOW_WIDTH               ((uint8_t)0x47)
#define RMC_TRG_MAX_DATA_BIT_CYCLE      ((uint8_t)0x83)
#define RMC_LARGER_THRESHOLD            ((uint8_t)0x63)
#define RMC_SMALLER_THRESHOLD           ((uint8_t)0x44)
#define RMC_NOISE_CANCELLATION_TIME     ((uint8_t)0x0F)
#endif

void RMC_Configuration(TSB_RMC_TypeDef * RMCx);

#endif                          /* __MAIN_H */
