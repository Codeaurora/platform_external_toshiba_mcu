/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SP_COMMON_H
#define _SP_COMMON_H

#include "TMPM330.h"
#include "tx03_common.h"
#include "sp_toshiba_common_utils.h"

#include "sp_debugcomm.h"
#include "tmpm330_tmrb.h"
#include "tmpm330_gpio.h"
#include "tmpm330_uart.h"

#define FALSE 0
#define TRUE  (!FALSE)

#define ON  0x1
#define OFF 0x0

/*
 * Macros to Enable/Disable Interrupts.
 */
#define INT_LOCK() __disable_irq();
#define INT_FREE() __enable_irq();

#endif /* _SP_COMMON_H */
