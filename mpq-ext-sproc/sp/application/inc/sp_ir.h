/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SP_IR_H
#define _SP_IR_H

#include "sp_common.h"
#include "mpq_standby_hostcomm.h"
#include "tmpm330_rmc.h"

/*
 * IR Protocol Configuration.
 */
//#define RC5_FORMAT_RMC
#define SAMSUNG_NEC_RMC

/*
 * Protocol Specific Timings.
 */
#ifdef RC5_FORMAT_RMC
#define RMC_POWER_KEY                  ((uint32_t)0x0C)
#define RMC_POWER_KEY_MASK             ((uint32_t)0xFF)
#define RMC_MAX_CYCLE                   ((uint8_t)0x00)
#define RMC_MIN_CYCLE                   ((uint8_t)0x00)
#define RMC_MAX_LOW_WIDTH               ((uint8_t)0x00)
#define RMC_MIN_LOW_WIDTH               ((uint8_t)0x00)
#define RMC_TRG_LOW_WIDTH               ((uint8_t)0x47)
#define RMC_TRG_MAX_DATA_BIT_CYCLE      ((uint8_t)0x83)
#define RMC_LARGER_THRESHOLD            ((uint8_t)0x63)
#define RMC_SMALLER_THRESHOLD           ((uint8_t)0x44)
#define RMC_NOISE_CANCELLATION_TIME     ((uint8_t)0x0F)
#endif

#ifdef SAMSUNG_NEC_RMC
#define RMC_POWER_KEY                  ((uint32_t)0x0070702)
#define RMC_POWER_KEY_MASK             ((uint32_t)0x00FFFFFF)
#define RMC_MAX_CYCLE                   ((uint8_t)0x51)
#define RMC_MIN_CYCLE                   ((uint8_t)0x3F)
#define RMC_MAX_LOW_WIDTH               ((uint8_t)0x2D)
#define RMC_MIN_LOW_WIDTH               ((uint8_t)0x1B)
#define RMC_TRG_LOW_WIDTH               ((uint8_t)0xFF)
#define RMC_TRG_MAX_DATA_BIT_CYCLE      ((uint8_t)0xFE)
#define RMC_LARGER_THRESHOLD            ((uint8_t)0x00)
#define RMC_SMALLER_THRESHOLD           ((uint8_t)0x2D)
#define RMC_NOISE_CANCELLATION_TIME     ((uint8_t)0x0F)
#endif

void SP_IRInit(void);
void SP_HandleIRKey(void);

#endif /* _SP_IR_H */
