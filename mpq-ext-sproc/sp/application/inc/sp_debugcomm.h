/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SP_DEBUGCOMM_H
#define _SP_DEBUGCOMM_H

#include <string.h>

#define SIZE_OF_DEBUG_BUFFER 50

void SP_DebugCommInit(void);

void SP_DebugUARTSendData(void);

extern unsigned char debug_tx_buffer[SIZE_OF_DEBUG_BUFFER];
#define SP_DebugPrint(...) \
{\
   sprintf((char *)debug_tx_buffer, __VA_ARGS__);\
   SP_DebugUARTSendData();\
}

#endif /* _SP_DEBUGCOMM_H */
