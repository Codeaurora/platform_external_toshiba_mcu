/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include "sp_common.h"
#include "sp_hostcomm.h"
#include "sp_debugcomm.h"
#include "sp_ir.h"
#include "sp_cec.h"
#include "tmpm330_cg.h"

volatile unsigned char sleep = TRUE;
volatile SP_MPQ_STATE mpq_state = MPQ_STATE_POWERDOWN;

int main(void)
{
   char loop_here = TRUE;

   /* Initialize Hardware */
   SP_HardwareInit();

   /* Wait for Supply ON. */
   while(!GPIO_ReadDataBit(GPIO_PC, GPIO_BIT_1));

   SP_GreenLEDPower(OFF);
   SP_RedLEDPower(OFF);

   /* Turn ON PMIC */
   SP_IssuePMICReset();

   /* Initialize Debug Communication. */
   SP_DebugCommInit();

   /* Initialize Host Communication module. */
   SP_HostCommInit();

   /* Initialize IR Device. */
   SP_IRInit();

   /* Initialize CEC. */
   SP_CECInit();

   //TODO: Don't Turn ON FRC by default.
   SP_FRCPowerOn();

   /* Infinite loop */
   while(loop_here)
   {
      SP_HostCommHandlePacket();

      SP_HandleCECInput();

      SP_HandleIRKey();

      SP_HandleHDMIHotplug();

      SP_HandleVGAHotplug();

      switch (mpq_state)
      {
         case MPQ_STATE_POWERDOWN:
            SP_RedLEDPower(ON);
            SP_GreenLEDPower(OFF);
            break;
         case MPQ_STATE_SUSPEND:
            SP_RedLEDPower(OFF);
            SP_GreenLEDPower(ON);
            break;
         case MPQ_STATE_ACTIVE:
            SP_RedLEDPower(ON);
            SP_GreenLEDPower(ON);
            break;
      }

   }

   return 0;
}
